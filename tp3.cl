;;;( (nom )(chanteur  )(langue  ) (genre  ) (emotion  ) (annee  ) (theme ))
(defclass $chanson ()
(($nom :accessor ?nom :initarg :nom)
($chanteur :accessor ?chanteur :initarg :chanteur)
($genre :accessor ?genre :initarg :genre)
($emotion :accessor ?emotion :initarg :emotion)
($annee :accessor ?annee :initarg :annee :type int)
($theme :accessor ?theme :initarg :theme  )
))




(setq chan1 (make-instance '$chanson :nom "won't cry" :chanteur "Jay Chou" :genre 'populaire :emotion 'romantique :annee 2019 :theme 'amour))
(setq chan2 (make-instance '$chanson :nom "Chega de saudade" :chanteur "Joao Gilberto" :genre 'BosaNova :emotion 'relaxation :annee 1958 :theme ()))
(setq chan3 (make-instance '$chanson :nom "Symphonie n? 25" :chanteur "Mozart" :genre 'Orchestra :emotion () :annee 1773 :theme () ))
(setq chan4 (make-instance '$chanson :nom "good time" :chanteur "Owl City" :genre 'Electro :emotion 'hereux :annee 2012 :theme ()))
(setq chan5 (make-instance '$chanson :nom "Strange Fruit" :chanteur "Billie Holiday" :genre 'jazz :emotion () :annee 1956 :theme () ))
(setq chan6 (make-instance '$chanson :nom "Hoochie Coochie Man" :chanteur "Muddy Waters" :genre 'blues :emotion 'triste :annee 1954 :theme () ))



(setq listeChanson (list chan1 chan2 chan3 chan4 chan5 chan6))



(setq BDR '(    ; Genre Annee
                    ( (Genre Jazz) (Annee min 1900) )  
                    ( (Genre Disco) (Annee min 1970) )
			        ( (Genre Pop) (Annee min 1960) )
		        	( (Genre Orchestra) (Annee max 1920) )
		        	( (Genre Rap) (Annee min 1970))
		        	
               
                
               ; Chanteur Annee
                     ( (Chanteur JayChou) (min 2003))
			     
				; genre Emotion
					( (Genre Jazz)  (Emotion happiness) )
					( (Genre Rap)   (Emotion anxiety violence))
					( (Genre Metal) (Emotion anxiety) )
					( (Genre Rock)  (Emotion anxiety))
					( (Genre Blues) (Emotion sadness))
					( (Genre Orchestra) (Emotion (not anxiety)))
					( (Genre CountryMusic) (Emotion sadness))
					
			   ;chanteur genre
					( (Chanteur ChrisWu) (Genre electronic))
					( (Chanteur JayChou) (Genre pop rap))
				;test
					( (Annee min 1900) (Genre test))
			
           )
)


(setq BDF '(
                (Annee 1950 1970 1950 1970);（用户 最小 最大 最小最大缺省值为-1）
                (emotion  happiness);()
                (Genre jazz)
                )
)


(defun affichageDonnees (donnees)
  (format t "~%~%-------------------------Donnees du client-------------------------~%")
  (dolist (item donnees)
    (format t "  ~s : ~{~a  ~} ~%" (car item) (cdr item) )
  )
)


(defun affichageResult (listchanson)
    (format t "~%~%-------------------------Resultat de chanson-------------------------")
    (dolist (item listchanson)
        (print (?nom item))

    )

)






;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun verifier(BDF)
    (and (verifierAnnee BDF) (verifierEmotion BDF))
)

 (defun erreur(phrase)
   (print phrase)
   NIL
)

(defun verifierAnnee(BDF)
  (let* ((item (assoc 'Annee BDF))
         (UserMin (nth 1 item))
         (UserMax (nth 2 item))
         (Min (nth 3 item))
         (Max (nth 4 item))
         )
    (if (and(<= UserMin UserMax) (<= Min UserMin) (<= UserMax Max))
      t
      (progn
       (print "Annee erreur")
        NIL
      )
      )
    )
 )
  
 
  (defun verifierEmotion (donnees)
	(setq emotionPositives '( happiness patriotism calm enthouiasm))
	(setq emotionNegatives '( sadness angry depress ))
		(let* ( (flag1 0) (flag2 0) ( emotions (cdr (assoc 'Emotion donnees) ) )  )

	
			(if (not (null emotions))
				(dolist (emotion emotions)
					(if (member emotion emotionPositives) (setq flag1 1))
					(if (member emotion emotionNegatives) (setq flag2 1))
				)
				T
			)
	
			(if  (or   (and (= flag1 0) (= flag2 1))  (and (= flag1 1) (= flag2 0)))    ;;toutes les emotions sont positives ou négatives
				donnees
                             (progn
                             (print "Les emotions ne sont pas toutes positives ou négatives. Deduction s'arret")
                               NIL
                             )
 
			)
	
		)
 )
 
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 


(defun updatedonnees (reglesgood donnees)
	(setq newdonnees '())
		(dolist (reglegood reglesgood)
			(update donnees (conclusion reglegood)) ;here (last reglegood) is the conclusion
		
		)


)


(defun inference (donnees BDR) ;donnees就是BDF
	
	(let* ( (donnes donnees) (baseregles BDR))
			(setq reglegoods '())
			(setq reglegoods (reglesverifies donnees baseregles))
			(format t "reglegoods : ~{ ~a ~% ~}~%" reglegoods) 
			;(print reglegoods)
			
			
			(setq baseregles '())
			(dolist (regle BDR) ;删除已经使用过的regle
				(if (not (member regle reglegoods :test #' equal))
						(pushnew regle baseregles)
				)
			)
			
			;(print "baseregles")
			;(print baseregles)
			
			(if (null reglegoods)
				T
			
				(progn
					
					(updatedonnees reglegoods donnees)
					(format t "reglegoods updated~%")
					(verifier donnees)
					;(print "222	")
					(inference donnees baseregles)
					;(print "333")
					(format t "fin de inference~%")
				)
			)
	)
)

;(inference donnees BDR)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 

(defun conclusion (item)
  (car (last item))
  )
  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  
  (defun update(BDF conclusion)
	(updateAnnee BDF conclusion)
	(updateEmotion BDF conclusion)
  )
  
  
 (defun updateAnnee (BDF conclusion)
  (if (equal (car conclusion) 'annee)
      (progn
        (if (equal (cadr conclusion) 'min)
            (setf (nth 3 (nth 0 BDF)) (car (last conclusion)))
        )
        (if (equal (cadr conclusion) 'max)
            (setf (nth 4 (nth 0 BDF)) (car (last conclusion)))
         )
      )
    t;跳出
  )    
  )
  
  
 (defun updateEmotion (donnees conclusion)
	(when (equal (car conclusion ) 'Emotion)
		(setf (nth 1 donnees) (append (nth 1 donnees) (cdr conclusion)))
	)
	donnees

)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun reglesverifies (donnees BDR)
	(setq reglesgood '())
	(setq d '())
	(let* ((baseregles BDR))
		(dolist (donnee donnees)
			(setq d (append d (cdr donnee)))
		)
		

	)
	
	(dolist (regle BDR)
		(setq f '())
		(setq faits (car regle))
		(if (not (listp (car faits)))
			(setq f (append f (cdr faits) ))
			
			(progn
				(dolist (fait faits)
					(setq f (append f (cdr fait)))
						
				)
				;(print f)												
			)
		)
		(setq flag 1)
		(mapcar #' (lambda (f) (if (not (member f d)) (setq flag 0))) f )
		;(print flag)
		(when (= flag 1)
			(pushnew regle reglesgood)
			(setq donnees (append donnees (last regle)))
		)
	
	
	)
	reglesgood
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun recherche(BDF listeChanson)
  (let* ( (result ())
        )


    (dolist (chanson listeChanson)
      (if (match chanson BDF)
        (pushnew chanson result)
      )
    )
    result
  )
)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun matchAnnee(annee BDF)
 (let* ((item (assoc 'Annee BDF))
         (UserMin (nth 1 item))
         (UserMax (nth 2 item))       
         )
  (and(<= UserMin annee) (<= annee UserMax))
    
 )
)
(defun matchgenre(genre BDF)
(let* ((item (assoc 'Genre BDF))
         (gen (nth 1 item))
         )
  (equal gen genre)   
 )
)
(defun matchEmotion(emotion BDF)
(if (equal emotion NIL)
  (return-from matchEmotion t)
)
(let* ((item (assoc 'emotion BDF))
         (emo (nth 1 item))
         )
  (equal emo emotion)   
 )
)


(defun match(chanson BDF)
  (and (matchAnnee (?annee chanson) BDF) 
       (matchGenre (?genre chanson) BDF)
        (matchEmotion (?emotion chanson) BDF)
  )

)

(defun recherche(BDF listeChanson)
  (let* ( (result ())
        )


    (dolist (chanson listeChanson)
      (if (match chanson BDF)
        (pushnew chanson result)
      )
    )
    result
  )
)




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun main()
  (affichageDonnees BDF)
  (if (verifier BDF);验证用户输入的basedefait有没有矛盾
      (progn
        (inference BDF BDR)
        (if (verifier BDF);验证basedefait有没有矛盾
            (progn
              (setq result (recherche BDF listeChanson));搜索数据库匹配
              (affichageResult result)
              (affichageDonnees BDF)
            )
            (erreur "aucune chanson a été trouvée! ")
        )
       )
      (erreur "contradiction entre les base de faits! ")
  )
 
)
